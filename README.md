# Proto Idl

Protocol Buffer Compiler Installation

1. Linux, using apt or apt-get, for example:

        $ apt install -y protobuf-compiler
        $ protoc --version  # Ensure compiler version is 3+

2. MacOS, using Homebrew:

        $ brew install protobuf
        $ protoc --version  # Ensure compiler version is 3+


To compile the protobuf follow the below commands for lanaguage


All lanaguages

        protoc --proto_path=IMPORT_PATH --cpp_out=DST_DIR --java_out=DST_DIR --python_out=DST_DIR --go_out=DST_DIR --ruby_out=DST_DIR --objc_out=DST_DIR --csharp_out=DST_DIR path/to/file.proto


Go Lanaguage

Reference : [Go Compiler](https://developers.google.com/protocol-buffers/docs/reference/go-generated)

        protoc profile/profile.proto --go_out=plugins=grpc:/Users/ashok/wavelabs/protobuf


Java Lanaguage
        
        protoc -I =/Users/ashok/wavelabs/protobuf/proto-idl/profile --java_out=/Users/ashok/wavelabs/protobuf /Users/ashok/wavelabs/protobuf/proto-idl/profile/profile.proto

Reference : [Java Compiler](https://developers.google.com/protocol-buffers/docs/reference/java-generated)



